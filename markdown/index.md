---
full-width: true
title: GStreamer
render-subpages: false
...


<div class="container">
<div class="page-header">
    <h1>GStreamer: a flexible, fast and multiplatform multimedia framework</h1>
    <p>
GStreamer는 스트리밍 미디어 어플리케이션을 만들기 위한 아주 강력하고 다목적 프레임워크 입니다.
GStreamer 프레임워크의 덕목 중 다수는 모듈화로부터 왔습니다: GStreamer는 새로운 플러그인 모듈들과 매끄럽게 통합될 수 있습니다.
그러나 모듈 성과 성능은 종종 더 복잡한 대가를 치르기 때문에 새로운 애플리케이션을 작성하는 것이 항상 쉬운 것은 아닙니다.
    </p>
    <a class="btn btn-default btn-xl page-scroll" href="tutorials/index.html" data-hotdoc-relative-link=true>Get Started</a>
</div>
</div>

<div class="row toned-row">
    <div class="col-lg-2 col-lg-offset-2 col-xs-6 col-md-3">
      <a class="icon" id="apiref" href="libs.html" data-hotdoc-relative-link=true>API Reference</a>
    </div>
    <div class="col-lg-2 col-xs-6 col-md-3">
      <a class="icon" id="hig" href="application-development/index.html" data-hotdoc-relative-link=true>
        Application developer manual
      </a>
    </div>
    <div class="col-lg-2 col-xs-6 col-md-3">
      <a class="icon" id="tutorials" href="tutorials/index.html" data-hotdoc-relative-link=true>
        Tutorials
      </a>
    </div>
    <div class="col-lg-2 col-xs-6 col-md-3">
      <a class="icon" id="deploy" href="deploying/index.html" data-hotdoc-relative-link=true>
        Deploying
      </a>
    </div>
</div>
